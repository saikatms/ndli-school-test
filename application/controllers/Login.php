<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
    // echo "Here we go";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
    }
    function _remap($method)
    {
        // echo $method;
        // echo "shdkjasgdk";
        switch ($method) {
                // case:
                // 
            default:
                $this->loginPage();
                break;
        }
    }

    public function loginPage()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('login_email', 'email', 'trim|required');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('sign_in');
                return true;
            }
            $this->auth_model->loginUser($this->input->post('login_email'), $this->input->post('password'));
            redirect('reading');
        }
        // die();
        $this->load->view('sign_in');
    }
}
