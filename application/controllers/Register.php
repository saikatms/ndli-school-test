<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
    }

    function _remap($method)
    {
        switch ($method) {
            default:
                $this->registerPage();
                break;
        }
    }
    public function registerPage()
    {
        if ($this->input->post()) {
            if ($this->validate($this->input->post())) {
                // print_r($this->input->post());
                // die();
                $data                   = $this->input->post();
                $data["password"]       = md5($this->input->post('password'));
                unset($data['confirm_password']);
                $this->db->insert('users', $data);
                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('message', 'Registration completed successfully.');
                redirect('login');
            } else {
                $this->load->view('register');
                return true;
            }
        }
        $this->load->view('register');
    }
    protected function validate($request)
    {
        $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('login_email', 'email', 'trim|required|valid_email|callback__duplicateEmailCheck|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'trim|required|callback__valid_password|xss_clean'); //
        $this->form_validation->set_rules('confirm_password', 'confirm password', 'trim|required|matches[password]|callback__valid_password|xss_clean'); //callback__valid_password
        $this->form_validation->set_rules('contact_no', 'contact no', 'trim|required|integer|xss_clean');
        if ($this->form_validation->run() == TRUE) {
            return true;
        }
        return false;
    }
    function _duplicateEmailCheck($email)
    {
        $email_exists = $this->isDuplicateEmailExists($email);
        $this->form_validation->set_message('_duplicateEmailCheck', 'The user email provided is already exists.');
        return $email_exists ? false : true;
    }
    /**
     * check dupliacte email exists 
     * @param $email
     */
    public function isDuplicateEmailExists($email)
    {
        $sql        = "SELECT `id` FROM `users` WHERE `login_email`  LIKE '%" . $this->db->escape_like_str($email) . "%' ";
        $query         = $this->db->query($sql);
        return $query->row();
    }

    /**
     * Validate the password     
     * @param string $password  
     * @return bool
     */
    public function _valid_password($password = '')
    {
        $password           = trim($password);
        $regex_lowercase    = '/[a-z]/';
        $regex_uppercase    = '/[A-Z]/';
        $regex_number       = '/[0-9]/';
        $regex_special      = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (preg_match_all($regex_lowercase, $password) < 1) {
            $this->form_validation->set_message('_valid_password', 'The {field} field must be at least one lowercase letter.');
            return FALSE;
        }
        if (preg_match_all($regex_uppercase, $password) < 1) {
            $this->form_validation->set_message('_valid_password', 'The {field} field must be at least one uppercase letter.');
            return FALSE;
        }
        if (preg_match_all($regex_number, $password) < 1) {
            $this->form_validation->set_message('_valid_password', 'The {field} field must have at least one number.');
            return FALSE;
        }
        if (preg_match_all($regex_special, $password) < 1) {
            $this->form_validation->set_message('_valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~'));
            return FALSE;
        }
        if (strlen($password) < 5) {
            $this->form_validation->set_message('_valid_password', 'The {field} field must be at least 5 characters in length.');
            return FALSE;
        }
        if (strlen($password) > 32) {
            $this->form_validation->set_message('_valid_password', 'The {field} field cannot exceed 32 characters in length.');
            return FALSE;
        }
        return TRUE;
    }
}
