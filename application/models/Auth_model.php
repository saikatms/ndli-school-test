<?php

class Auth_model extends CI_Model
{

    function loginUser($user_email, $password)
    {
        $sql         = "SELECT * FROM `users` WHERE `login_email` = ? AND `status` = 1 AND `user_type` = 0";
        $query         = $this->db->query($sql, array($user_email));
        $numrows     = $query->num_rows();
        $row         = $query->row();
        $login_status     = FALSE;

        if ($numrows > 0) {

            if ($row->password == $password) {

                $login_status     = TRUE;
                $llogintime         = date("D, F j, Y, g:i a");
                $user_id             = $row->id;
                # Setting session for authenticated user
                $credentials = array(
                    'user_login_status'         => $login_status,
                    'log_user_id'             => $row->id,
                    'log_user_name'             => $row->name
                );
                $this->session->set_userdata($credentials);
                $this->db->where('id', $user_id);
                $this->db->set('last_login_ip', $_SERVER['REMOTE_ADDR']);
                $this->db->set('last_login_time', $llogintime);
                $this->db->update('users');
                redirect('home');
            } else {
                $this->session->set_flashdata('msg_type', 'danger');
                $this->session->set_flashdata('message', 'user name or password you entered is incorrect.');
                redirect('login');
            }
        } else {
            $this->session->set_flashdata('msg_type', 'danger');
            $this->session->set_flashdata('message', 'user name or password you entered is incorrect.');
            redirect('login');
        }
    }
}
