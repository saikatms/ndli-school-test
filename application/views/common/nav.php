<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style="background: #dae0e5!important;">
   <a class="navbar-brand" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/admin/images/logo-lg-green.svg" class="img-responsive" alt="National Digital Library of India" style="width:100px"></a>
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
   </button>
   <div class="collapse navbar-collapse" >
      <ul class="nav ml-auto">
      <?php if ($this->session->userdata('log_user_name')) {?>
         <li class="nav-item"><a href="#" class="nav-link">Welcome, <?php echo $this->session->userdata('log_user_name');?></a></li>
         <li class="nav-item"><a href="<?php echo site_url('login/logout');?>" class="nav-link"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
         <?php } else {?>
         <li class="nav-item"><a href="<?php echo site_url('register');?>" class="nav-link"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign Up</a></li>
         <li class="nav-item"><a href="<?php echo site_url('login');?>" class="nav-link"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a></li>
         <?php }?>
        </ul>
   </div>
</nav>
