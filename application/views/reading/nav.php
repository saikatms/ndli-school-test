<!-- <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style="background: #dae0e5!important;">
   <a class="navbar-brand" href="javascript:void(0);"><img src="<?php echo base_url(); ?>assets/admin/images/logo-lg-green.svg" class="img-responsive" alt="National Digital Library of India" style="width:100px"></a>
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
   </button>
   <div class="collapse navbar-collapse" >
      <ul class="nav ml-auto">
      <?php if ($this->session->userdata('log_user_name')) { ?>
         <li class="nav-item"><a href="#" class="nav-link">Welcome, <?php echo $this->session->userdata('log_user_name'); ?></a></li>
         <li class="nav-item"><a href="<?php echo site_url('login/logout'); ?>" class="nav-link"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
         <?php } else { ?>
         <li class="nav-item"><a href="<?php echo site_url('register'); ?>" class="nav-link"><i class="fa fa-user-plus" aria-hidden="true"></i> Sign Up</a></li>
         <li class="nav-item"><a href="<?php echo site_url('login'); ?>" class="nav-link"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</a></li>
         <?php } ?>
        </ul>
   </div>
</nav> -->
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style="background: #e8cfb0!important;">
    <a class="navbar-brand" href="javascript:void(0);"><img src="<?php echo base_url(); ?>assets/admin/images/logo-lg-green.svg" class="img-responsive" alt="National Digital Library of India" style="width:100px"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>

        <div>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
        </div>

    </div>
</nav>