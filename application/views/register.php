<!DOCTYPE html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>         
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>         
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
<!--<![endif]-->
<?php $this->load->view('common/head');?>
<body>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <?php $this->load->view('common/nav');?>
    <div class="container">
        <?php echo form_open_multipart('register', array('id' => 'regform','name' => 'regform','class' => 'well form-horizontal'));?>
        <!-- Form Name -->
        <legend><center><h2><b>Sign Up </b></h2> </center></legend>
        <br>
        <!-- Text input-->
        <div class="form-group row">
            <label for="Name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
            <input name="name" placeholder="Full Name" class="form-control" type="text" value="<?php echo $this->input->post('name', TRUE)?>" autocomplete="off" aria-describedby="Name">
            <small id="Name" class="form-text error text-danger"><?php echo form_error('name'); ?></small>
            </div>
        </div>
        <div class="form-group row">
            <label for="E-Mail" class="col-sm-2 col-form-label">E-Mail</label>
            <div class="col-sm-10">
            <input name="login_email" placeholder="E-Mail Address" class="form-control"  type="text" value="<?php echo $this->input->post('login_email', TRUE)?>" autocomplete="off">
            <small id="E-Mail" class="form-text error text-danger"><?php echo form_error('login_email'); ?></small>
            </div>
        </div>
        <div class="form-group row">
            <label for="Password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
            <input name="password" placeholder="Password" class="form-control"  type="password" autocomplete="off">
            <small id="Password" class="form-text error text-danger"><?php echo form_error('password'); ?></small>
            </div>
        </div>
        <div class="form-group row">
            <label for="ConfirmPassword" class="col-sm-2 col-form-label">Confirm Password</label>
            <div class="col-sm-10">
            <input name="confirm_password" placeholder="Password" class="form-control"  type="password" autocomplete="off">
            <small id="ConfirmPassword" class="form-text error text-danger"><?php echo form_error('confirm_password'); ?></small>
            </div>
        </div>
        <div class="form-group row">
            <label for="Contact No." class="col-sm-2 col-form-label">Contact No.</label>
            <div class="col-sm-10">
            <input name="contact_no" placeholder="Contact No" class="form-control"  type="text" value="<?php echo $this->input->post('contact_no', TRUE)?>" autocomplete="off">
            <small id="Contact No." class="form-text error text-danger"><?php echo form_error('contact_no'); ?></small>
            </div>
        </div>
        <!-- Button -->
        <button type="submit" class="btn btn-warning" style="float:right;">SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        <?php echo form_close();?> 
    </div>
    </div><!-- /.container -->
</body>
</html>