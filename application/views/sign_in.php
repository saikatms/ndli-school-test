<!DOCTYPE html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>         
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>         
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<?php $this->load->view('common/head'); ?>

<body>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <?php $this->load->view('common/nav'); ?>
    <div class="container">
        <?php echo form_open_multipart('login', array('id' => 'loginform', 'name' => 'loginform', 'class' => 'well form-horizontal')); ?>
        <fieldset>
            <!-- Form Name -->
            <legend>
                <center>
                    <h2><b>Sign In </b></h2>
                </center>
            </legend>
            <br>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label">E-Mail</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input name="login_email" placeholder="E-Mail Address" class="form-control" type="text" value="<?php echo $this->input->post('login_email', TRUE) ?>" autocomplete="off">
                        <span class="error mt-2 text-danger" for="login_email"><?php echo form_error('login_email'); ?></span>
                    </div>
                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label">Password</label>
                <div class="col-md-4 inputGroupContainer">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input name="password" id="password" placeholder="Password" class="form-control" type="password">
                        <span class="error mt-2 text-danger" for="password"><?php echo form_error('password'); ?></span>
                    </div>
                </div>
            </div>
            <!-- Success message -->
            <?php if ($this->session->flashdata('message')) { ?>
                <div class="alert alert-<?php echo $this->session->flashdata('msg_type'); ?>" role="alert" id="success_message"> <i class="glyphicon glyphicon-thumbs-up"></i> <?php echo $this->session->flashdata('message'); ?></div>
            <?php } ?>
            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4"><button type="submit" class="btn btn-warning" id="frmsubmit">SUBMIT <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
    </div><!-- /.container -->
</body>

<script src="<?php echo base_url(); ?>assets/admin/js/jquery.md5.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $("#frmsubmit").click(function(event) {
        event.preventDefault();
        $('#password').val($.MD5($('#password').val()));
        $("#loginform").submit();
    });
</script>

</html>